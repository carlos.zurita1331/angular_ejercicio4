import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  anioActual = new Date;
  diasemana = this.anioActual.getDay();
  dia = this.anioActual.getDate();
  mes = this.anioActual.getMonth();
  anio = this.anioActual.getFullYear();


  DIAS = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
  MESES = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

 fecha = this.DIAS[this.diasemana]+" "+this.dia + " de "+this.MESES[this.mes]+" de "+ this.anio;

  constructor() { }

  ngOnInit(): void {
  }

}
